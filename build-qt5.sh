#!/usr/bin/env bash
set -xeuo pipefail
cd "$(dirname -- "$(readlink -f -- "$0")")"

ROOTDIR="$(pwd)"
SHADOWDIR="$ROOTDIR/shadow"
DISTDIR="$ROOTDIR/dist"
NPROC="$(nproc)"

export LLVM_INSTALL_DIR=/usr/lib/llvm-6.0
export QT_INSTALL_HEADERS="$DISTDIR/include"
export PATH="$DISTDIR/bin${PATH:+:$PATH}"
export LD_LIBRARY_PATH="$DISTDIR/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"

function doBootstrap() {
    PACKAGES=(
        git
        build-essential
        python
        pkg-config
        cmake
        libclang-dev
        libfontconfig1-dev
        libwayland-dev
        libwayland-egl1-mesa
        libegl1-mesa-dev
        mesa-common-dev
        libssl-dev
        libxkbcommon-dev
    )

    MISSING="$(apt list -qq --no-installed "${PACKAGES[@]}" | grep -vP '\[installed\]$' | wc -l)" || echo 0
    if [[ $MISSING != 0 ]]
    then
        if [[ -t 1 ]]
        then
            YFLAG=""
        else
            YFLAG="-y"
        fi
        if [[ $EUID != 0 ]]
        then
            sudo apt install $YFLAG "${PACKAGES[@]}"
        else
            apt install $YFLAG "${PACKAGES[@]}"
        fi
    fi
}

function doInit() {
    TAG="$1"
    if [[ -z $TAG ]]
    then
        echo "specify the git tag as an additional argument"
        echo "find the list of tags at https://code.qt.io/cgit/qt/qt5.git/refs/"
        exit -1
    fi

    PATCHFILES=(qtbase qtdeclarative)
    for PATCHFILENAME in "${PATCHFILES[@]}"
    do
        PATCHFILE="$ROOTDIR/$PATCHFILENAME.patch"
        if [[ ! -f $PATCHFILE ]]
        then
            echo "$PATCHFILE not found"
            exit -1
        fi
    done

    rm -rf qt5
    git clone https://code.qt.io/qt/qt5.git
    pushd qt5
        ./init-repository --module-subset=qtbase,qtcharts,qtdeclarative,qtquickcontrols,qtquickcontrols2,qtgraphicaleffects,qttools,qttranslations,qtwayland
        git checkout "$TAG"
        git submodule update
        for PATCHFILENAME in "${PATCHFILES[@]}"
        do
            pushd $PATCHFILENAME
                git apply "$ROOTDIR/$PATCHFILENAME.patch"
            popd
        done
    popd
}

function doConfigure() {
    rm -rf "$SHADOWDIR"
    mkdir -p "$SHADOWDIR"
    pushd "$SHADOWDIR"

    "$ROOTDIR/qt5/configure" \
        -prefix "$DISTDIR" \
        -opensource \
        -confirm-license \
        -reduce-exports \
        -reduce-relocations \
        -silent \
        -make libs \
        -make tools \
        -no-glib \
        -no-icu \
        -opengl desktop \
        -system-zlib \
        -qt-doubleconversion \
        -qt-pcre \
        -fontconfig \
        -system-freetype \
        -qt-harfbuzz \
        -qt-libjpeg \
        -qt-libpng \
        -qt-xcb \
        -qt-sqlite \
        -openssl

    popd
}

function doBuild() {
    pushd "$SHADOWDIR"
    make -j"$NPROC" > /dev/null
    rm -rf "$DISTDIR"
    make install > /dev/null
    popd

    cat > "$DISTDIR/bin/qt.conf" << EOF
[Paths]
Prefix=..
EOF
}

function doDocs() {
    pushd "$SHADOWDIR"
    make -j"$NPROC" docs > /dev/null
    make install_qch_docs > /dev/null
    popd
}

case "${1:-}" in
    bootstrap) doBootstrap ;;
    init) doInit "${2:-}" ;;
    configure) doConfigure ;;
    build) doBuild ;;
    docs) doDocs ;;
    all)
        doBootstrap
        doInit "${2:-}"
        doConfigure
        doBuild
        doDocs
        ;;
    *)
        echo "specify a valid task (bootstrap, init, configure, build, docs, all) as a first argument"
        exit -1
        ;;
esac

echo "DONE!"
