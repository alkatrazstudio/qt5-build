#!/usr/bin/env bash
set -euo pipefail
cd "$(dirname -- "$(readlink -f -- "$0")")"

TAG="${1:-}"
if [[ -z $TAG ]]
then
    TAG="$(git ls-remote --tags https://code.qt.io/qt/qt5.git | grep -v '[\^-]' | sort -t '/' -k 3 -V | tail -1 | sed 's/.*\///; s/\^{}//')"
    echo "----------------------------------"
    echo "Latest stable Qt version: $TAG"
    echo "----------------------------------"
    echo
    echo "Press \"Enter\" to use this version."
    echo
    echo "Or press CTRL+C and specify the Qt git tag as an argument to this script."
    echo "(i.e. \"./build-all.sh v5.12.2\")"
    echo "Find the list of Qt git tags at https://code.qt.io/cgit/qt/qt5.git/refs/"
    echo
    read
fi

./build-qt5.sh all "$TAG"
./build-libs.sh
./build-qbs.sh

echo "ALL DONE!"
