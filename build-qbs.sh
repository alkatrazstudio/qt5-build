#!/usr/bin/env bash
set -xeuo pipefail
cd "$(dirname -- "$(readlink -f -- "$0")")"

THISDIR="$(pwd)"
DISTDIR="$THISDIR/dist"
export PATH="$DISTDIR/bin${PATH:+:$PATH}"
export LD_LIBRARY_PATH="$DISTDIR/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
NPROC="$(nproc)"

rm -rf qbs
git clone https://code.qt.io/qbs/qbs.git
pushd qbs
    git checkout "$(git describe --abbrev=0 --tags)"
    git submodule update --init
    qmake
    make -j"$NPROC" > /dev/null
    find -name 'Makefile*' -type f -exec sed -i 's/$(INSTALL_ROOT)\/usr\/local\//$(INSTALL_ROOT)\//g' {} +
    make install INSTALL_ROOT="$DISTDIR" > /dev/null
    find -name 'Makefile*' -type f -exec sed -i 's/$(INSTALL_ROOT)\/share\/doc\//$(INSTALL_ROOT)\/doc\//g' {} +
    make qch_docs install_inst_qch_docs INSTALL_ROOT="$DISTDIR" > /dev/null
popd

echo "DONE!"
