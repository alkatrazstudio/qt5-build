#!/usr/bin/env bash
set -xeuo pipefail
cd "$(dirname -- "$(readlink -f -- "$0")")"

THISDIR="$(pwd)"
DISTDIR="$THISDIR/dist"
export PATH="$DISTDIR/bin${PATH:+:$PATH}"
export LD_LIBRARY_PATH="$DISTDIR/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
export PKG_CONFIG_PATH="$DISTDIR/lib/pkgconfig${PKG_CONFIG_PATH:+:$PKG_CONFIG_PATH}"
export CMAKE_PREFIX_PATH="$THISDIR${CMAKE_PREFIX_PATH:+:$CMAKE_PREFIX_PATH}"
NPROC="$(nproc)"

#
# QuaZip
#
rm -rf quazip
git clone https://github.com/stachenov/quazip.git
pushd quazip/quazip
    git checkout "$(git describe --abbrev=0 --tags)"
    qmake PREFIX="$DISTDIR"
    make -j"$NPROC" > /dev/null
    make install > /dev/null
popd

#
# QtDBusExtended
#
rm -rf qtdbusextended
git clone https://github.com/nemomobile/qtdbusextended.git
pushd qtdbusextended
    git checkout "$(git describe --abbrev=0 --tags)"
    qmake
    make -j"$NPROC" > /dev/null
    make install > /dev/null
popd

#
# QtMPRIS
#
rm -rf qtmpris
git clone https://git.merproject.org/mer-core/qtmpris.git
pushd qtmpris
    git checkout "$(git describe --abbrev=0 --tags)"
    qmake
    make -j"$NPROC" > /dev/null
    make install > /dev/null
popd

#
# ECM
#
rm -rf extra-cmake-modules
git clone https://github.com/KDE/extra-cmake-modules.git
pushd extra-cmake-modules
    KF_TAG="$(git tag | grep -v '[\^-]' | sort -V | tail -1)"
    git checkout "$KF_TAG"
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX="$DISTDIR" ..
    make -j"$NPROC" > /dev/null
    make install > /dev/null
popd

#
# KSyntaxHighlighting
#
rm -rf syntax-highlighting
git clone https://github.com/KDE/syntax-highlighting.git
pushd syntax-highlighting
    git checkout "$KF_TAG"
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX="$DISTDIR" ..
    make -j"$NPROC" > /dev/null
    make install > /dev/null
popd

cp -a "$DISTDIR/lib/x86_64-linux-gnu/." "$DISTDIR/lib/"
rm -rf "$DISTDIR/lib/x86_64-linux-gnu/"

echo "DONE!"
